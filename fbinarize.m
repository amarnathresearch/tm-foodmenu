function [ c ] = fbinarize( c )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
    [fh, fw] = size(c);
    for i = 1:fh
        for j=1:fw
            c(i, j).c = histeq(c(i, j).c);
        end
    end
end