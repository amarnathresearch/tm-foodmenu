function [ out ] = fdiv( bw, offh, offw )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    [h, w] = size(bw);
    noh = ceil(h/offh);
    now = ceil(w/offw);
    sh = 1;
    eh = offh;
    for i=1:noh
        sw = 1;
        ew = offw;
        if eh > h
            eh = h;
        end
        if sh >= h
            break;
        end
        for j=1:now
            if ew > w
                ew = w;
            end
            if sw >= w
                break;
            end
            c(i, j).c = bw(sh:eh, sw:ew);
            sw = ew + 1;
            ew = ew + offw;
            
        end
        sh = eh + 1;
        eh = eh + offh;
    end
    out = c;
end

