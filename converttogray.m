function [ gray ] = converttogray( gray, dil )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    [h, w] = size(dil);
    for i=1:h
        for j=1:w
            if dil (i, j) == 0
                gray(i, j, :) = 255;
            end
        end
    end
end

