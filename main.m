clear all;
close all;
clc;
filepath = 'F:\textmercato\sample.png';
outputfilename = 'sample-out.png';
original = imread(filepath);
figure, imshow(original);
gray = rgb2gray(original);
figure, imshow(gray), title('gray');

% fd = fdiv( gray, 150, 452 );
% fb = fbinarize(fd);
% fc = fcombine(fb);
% figure, imshow(fc);


cn = edge(gray, 'canny');
figure, imshow(cn);

se = strel('square', 4);
dil = imdilate(cn, se);
figure, imshow(dil), title('dil');
fcon = findconnect( dil, gray );
figure, imshow(fcon);

bw = converttobw( original, dil );
figure, imshow(bw), title('original');
imwrite(bw, outputfilename);

gray = converttogray( gray, dil );
figure, imshow(gray);



% he = histeq(gray);
% figure, imshow(he);

% fb = filterblack( he, 30 );
% 
% figure, imshow(fb);
