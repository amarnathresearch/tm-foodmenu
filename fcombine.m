function [ out ] = fcombine( c )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
    
    [fh, fw] = size(c);
    out = c(1, 1).c;
    for j = 2:fw
        out = horzcat(out, c(1, j).c);
    end
    for i = 2:fh
        t = c(i, 1).c;
        for j=2:fw
            t = horzcat(t, c(i, j).c);
        end
        out = vertcat(out, t);
    end
end

