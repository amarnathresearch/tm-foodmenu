function [ img ] = findconnect( bw, gray )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
    [h, w] = size(bw);

    [cc, n] = bwlabel(bw, 4);
    st = zeros(n, 1);
    for c = 1: n
%         c
        froi = findroi( cc, gray, c );
        st(c, 1) = std(froi);
    end
    
    [h, w] = size(bw);
    img = zeros(h, w);
    for c = 1:n
        if st(c, 1) < 10
            c
            for i=1:h
                for j=1:w
                    if cc(i, j) == c
                        img(i, j) = gray(i, j);
                    end
                end
            end
        end
    end
end

